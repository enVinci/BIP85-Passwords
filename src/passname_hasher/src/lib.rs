// src/passname_hasher/lib.rs

// use std::collections::hash_map::DefaultHasher;
// use std::hash::{Hash, Hasher};

#[derive(Clone)]
pub struct PassNameHasherConfig {
    pub init_hash: u32,
    pub space_size: u32,
    pub prime: u32,
    pub arg1: u32,
    pub arg2: u32,
}

impl PassNameHasherConfig {
    pub const SIZE: usize = std::mem::size_of::<u32>() * 5 + 8; // 5 u32 fields + 4 bytes for start marker + 4 bytes for end marker
    const START_MARKER: [u8; 4] = [0x48, 0x41, 0x53, 0x48]; // ASCII for "HASH"
    const END_MARKER: [u8; 4] = [0x43, 0x4F, 0x4E, 0x46]; // ASCII for "CONF"

    pub fn format_fields(&self) -> String {
        format!(
            "{}.{}.{}.{}.{}",
            self.init_hash, self.space_size, self.prime, self.arg1, self.arg2
        )
    }

    // Encode the struct to a fixed-size byte array
    pub fn to_bytes(&self) -> [u8; Self::SIZE] {
        let mut bytes = [0u8; Self::SIZE];
        bytes[0..4].copy_from_slice(&Self::START_MARKER);
        bytes[4..8].copy_from_slice(&self.init_hash.to_le_bytes());
        bytes[8..12].copy_from_slice(&self.space_size.to_le_bytes());
        bytes[12..16].copy_from_slice(&self.prime.to_le_bytes());
        bytes[16..20].copy_from_slice(&self.arg1.to_le_bytes());
        bytes[20..24].copy_from_slice(&self.arg2.to_le_bytes());
        bytes[24..28].copy_from_slice(&Self::END_MARKER);
        bytes
    }

    // Decode the struct from a fixed-size byte array
    pub fn from_bytes(bytes: &[u8; Self::SIZE]) -> Self {
        // Check for start and end markers
        if &bytes[0..4] != &Self::START_MARKER || &bytes[24..28] != &Self::END_MARKER {
            panic!("Invalid byte data format: missing hasher config markers");
        }

        let init_hash = u32::from_le_bytes(bytes[4..8].try_into().unwrap());
        let space_size = u32::from_le_bytes(bytes[8..12].try_into().unwrap());
        let prime = u32::from_le_bytes(bytes[12..16].try_into().unwrap());
        let arg1 = u32::from_le_bytes(bytes[16..20].try_into().unwrap());
        let arg2 = u32::from_le_bytes(bytes[20..24].try_into().unwrap());

        Self {
            init_hash,
            space_size,
            prime,
            arg1,
            arg2,
        }
    }
}

pub trait Hashable {
    fn new(config: PassNameHasherConfig) -> Self;
    fn hash(&self, arg2: &str) -> u32;
    fn name(&self) -> String;
}

pub struct PassNameHasher {
    config: PassNameHasherConfig,
}

impl PassNameHasher {
    //     pub fn hash_function_default(input: &str, space_size: u32) -> u32 {
    //         let mut hasher = DefaultHasher::new();
    //         input.hash(&mut hasher);
    //         (hasher.finish() % space_size as u64).try_into().unwrap()
    //     }

    //     fn hash_function_fnv(input_string: &str) -> u32 {
    //         const PRIME: u32 = 101; // Multiplier, it does not have to be a prime number
    //         const STATE_SPACE: u32 = 10000;
    //         let mut hash: u32 = 0; // FNV offset basis, may be 0xCAFEBABE to easy remember
    //         // Fowler–Noll–Vo (FNV) hash algorithm
    //         for ch in input_string.chars() {
    //             hash ^= ch as u32;
    //             hash = hash.wrapping_mul(PRIME);
    //         }
    //
    //         hash % STATE_SPACE
    //     }

    //     fn hash_function_polyu32(input_string: &str) -> u32 {
    //         const PRIME: u32 = 31;
    //         const STATE_SPACE: u32 = 10000; // Hash space size
    //         let mut hash: u32 = 0;
    //         let mut power: u32 = 1;
    //
    //         for ch in input_string.chars() {
    //             hash = hash.wrapping_add(power.wrapping_mul(ch as u32));
    //             power = power.wrapping_mul(PRIME);
    //         }
    //
    //         hash % STATE_SPACE
    //     }

    //     fn hash_function_polyu32_u64internal(input_string: &str) -> u32 {
    //         const PRIME: u64 = 31;
    //         const STATE_SPACE: u32 = 10000; // Hash space size
    //         let mut hash: u64 = 0;
    //         let mut power: u64 = 1;
    //
    //         for ch in input_string.chars() {
    //             hash = hash.wrapping_add(power.wrapping_mul(ch as u64));
    //             power = power.wrapping_mul(PRIME);
    //         }
    //
    //         (hash % (STATE_SPACE as u64)) as u32 // Cast the final result back to u32
    //     }

    fn hash_function_polyu32(
        input_string: &str,
        init_hash: u32,
        prime: u32,
        space_size: u32,
    ) -> u32 {
        let mut hash: u32 = init_hash;
        let mut power: u32 = 1;

        for ch in input_string.chars() {
            hash = hash.wrapping_add(power.wrapping_mul(ch as u32));
            power = power.wrapping_mul(prime);
        }

        hash % space_size
    }

    fn get_name() -> String {
        String::from("hash_function_polyu32")
    }
}

impl Hashable for PassNameHasher {
    fn new(config: PassNameHasherConfig) -> Self {
        PassNameHasher { config }
    }

    fn hash(&self, input_string: &str) -> u32 {
        //         PassNameHasher::hash_function_default(input_string, self.config.space_size)
        PassNameHasher::hash_function_polyu32(
            input_string,
            self.config.init_hash,
            self.config.prime,
            self.config.space_size,
        )
    }

    fn name(&self) -> String {
        PassNameHasher::get_name()
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    // Define a common configuration to be reused in tests
    fn default_config() -> PassNameHasherConfig {
        PassNameHasherConfig {
            init_hash: 0,
            space_size: 10000,
            prime: 31,
            arg1: 0,
            arg2: 0,
        }
    }

    fn test_hash_config() -> PassNameHasherConfig {
        PassNameHasherConfig {
            init_hash: 12345,
            space_size: 100,
            prime: 31,
            arg1: 42,
            arg2: 7,
        }
    }

    #[test]
    fn test_hash_polyu32_empty_string() {
        let config = default_config();
        let hasher = PassNameHasher::new(config);
        assert_eq!(hasher.hash(""), 0);
    }

    #[test]
    fn test_hash_polyu32_single_character() {
        let config = default_config();
        let hasher = PassNameHasher::new(config);
        // Testing lowercase letters
        assert_eq!(hasher.hash("a"), 97); // ASCII value of 'a' is 97
        assert_eq!(hasher.hash("b"), 98); // ASCII value of 'b' is 98
        assert_eq!(hasher.hash("c"), 99); // ASCII value of 'c' is 99
        assert_eq!(hasher.hash("d"), 100); // ASCII value of 'd' is 100
        assert_eq!(hasher.hash("e"), 101); // ASCII value of 'e' is 101
        assert_eq!(hasher.hash("f"), 102); // ASCII value of 'f' is 102
        assert_eq!(hasher.hash("g"), 103); // ASCII value of 'g' is 103
        assert_eq!(hasher.hash("h"), 104); // ASCII value of 'h' is 104
        assert_eq!(hasher.hash("i"), 105); // ASCII value of 'i' is 105
        assert_eq!(hasher.hash("j"), 106); // ASCII value of 'j' is 106
        assert_eq!(hasher.hash("k"), 107); // ASCII value of 'k' is 107
        assert_eq!(hasher.hash("l"), 108); // ASCII value of 'l' is 108
        assert_eq!(hasher.hash("m"), 109); // ASCII value of 'm' is 109
        assert_eq!(hasher.hash("n"), 110); // ASCII value of 'n' is 110
        assert_eq!(hasher.hash("o"), 111); // ASCII value of 'o' is 111
        assert_eq!(hasher.hash("p"), 112); // ASCII value of 'p' is 112
        assert_eq!(hasher.hash("q"), 113); // ASCII value of 'q' is 113
        assert_eq!(hasher.hash("r"), 114); // ASCII value of 'r' is 114
        assert_eq!(hasher.hash("s"), 115); // ASCII value of 's' is 115
        assert_eq!(hasher.hash("t"), 116); // ASCII value of 't' is 116
        assert_eq!(hasher.hash("u"), 117); // ASCII value of 'u' is 117
        assert_eq!(hasher.hash("v"), 118); // ASCII value of 'v' is 118
        assert_eq!(hasher.hash("w"), 119); // ASCII value of 'w' is 119
        assert_eq!(hasher.hash("x"), 120); // ASCII value of 'x' is 120
        assert_eq!(hasher.hash("y"), 121); // ASCII value of 'y' is 121
        assert_eq!(hasher.hash("z"), 122); // ASCII value of 'z' is 122

        // Testing uppercase letters
        assert_eq!(hasher.hash("A"), 65); // ASCII value of 'A' is 65
        assert_eq!(hasher.hash("B"), 66); // ASCII value of 'B' is 66
        assert_eq!(hasher.hash("C"), 67); // ASCII value of 'C' is 67
        assert_eq!(hasher.hash("D"), 68); // ASCII value of 'D' is 68
        assert_eq!(hasher.hash("E"), 69); // ASCII value of 'E' is 69
        assert_eq!(hasher.hash("F"), 70); // ASCII value of 'F' is 70
        assert_eq!(hasher.hash("G"), 71); // ASCII value of 'G' is 71
        assert_eq!(hasher.hash("H"), 72); // ASCII value of 'H' is 72
        assert_eq!(hasher.hash("I"), 73); // ASCII value of 'I' is 73
        assert_eq!(hasher.hash("J"), 74); // ASCII value of 'J' is 74
        assert_eq!(hasher.hash("K"), 75); // ASCII value of 'K' is 75
        assert_eq!(hasher.hash("L"), 76); // ASCII value of 'L' is 76
        assert_eq!(hasher.hash("M"), 77); // ASCII value of 'M' is 77
        assert_eq!(hasher.hash("N"), 78); // ASCII value of 'N' is 78
        assert_eq!(hasher.hash("O"), 79); // ASCII value of 'O' is 79
        assert_eq!(hasher.hash("P"), 80); // ASCII value of 'P' is 80
        assert_eq!(hasher.hash("Q"), 81); // ASCII value of 'Q' is 81
        assert_eq!(hasher.hash("R"), 82); // ASCII value of 'R' is 82
        assert_eq!(hasher.hash("S"), 83); // ASCII value of 'S' is 83
        assert_eq!(hasher.hash("T"), 84); // ASCII value of 'T' is 84
        assert_eq!(hasher.hash("U"), 85); // ASCII value of 'U' is 85
        assert_eq!(hasher.hash("V"), 86); // ASCII value of 'V' is 86
        assert_eq!(hasher.hash("W"), 87); // ASCII value of 'W' is 87
        assert_eq!(hasher.hash("X"), 88); // ASCII value of 'X' is 88
        assert_eq!(hasher.hash("Y"), 89); // ASCII value of 'Y' is 89
        assert_eq!(hasher.hash("Z"), 90); // ASCII value of 'Z' is 90

        // Testing some special characters
        assert_eq!(hasher.hash("!"), 33); // ASCII value of '!' is 33
        assert_eq!(hasher.hash("@"), 64); // ASCII value of '@' is 64
        assert_eq!(hasher.hash("#"), 35); // ASCII value of '#' is 35
        assert_eq!(hasher.hash("$"), 36); // ASCII value of '$' is 36
        assert_eq!(hasher.hash("%"), 37); // ASCII value of '%' is 37
        assert_eq!(hasher.hash("^"), 94); // ASCII value of '^' is 94
        assert_eq!(hasher.hash("&"), 38); // ASCII value of '&' is 38
        assert_eq!(hasher.hash("("), 40); // ASCII values of '('
        assert_eq!(hasher.hash(")"), 41); // ASCII values of ')'
        assert_eq!(hasher.hash("*"), 42); // ASCII value of '*' is 42
    }

    #[test]
    fn test_hash_polyu32_unicode_single_characters() {
        let config = default_config();
        let hasher = PassNameHasher::new(config);
        // Testing some common Unicode characters
        assert_eq!(hasher.hash("é"), 233); // Unicode value for 'é' is 233
        assert_eq!(hasher.hash("ñ"), 241); // Unicode value for 'ñ' is 241
        assert_eq!(hasher.hash("中"), 20013 % 10000); // Unicode value for '中' (Chinese character) is 20013
        assert_eq!(hasher.hash("α"), 945); // Unicode value for 'α' (Greek letter alpha) is 945
        assert_eq!(hasher.hash("β"), 946); // Unicode value for 'β' (Greek letter beta) is 946
        assert_eq!(hasher.hash("א"), 1488); // Unicode value for 'א' (Hebrew letter Aleph) is 1488
        assert_eq!(hasher.hash("ב"), 1489); // Unicode value for 'ב' (Hebrew letter Bet)
                                            // Testing some common emojis
        assert_eq!(hasher.hash("😀"), 128512 % 10000); // Grinning face
        assert_eq!(hasher.hash("😂"), 128514 % 10000); // Face with tears of joy
        assert_eq!(hasher.hash("😍"), 128525 % 10000); // Smiling face with heart-eyes
        assert_eq!(hasher.hash("😎"), 128526 % 10000); // Smiling face with sunglasses
        assert_eq!(hasher.hash("😢"), 128546 % 10000); // Crying face
        assert_eq!(hasher.hash("😡"), 128545 % 10000); // Angry face
        assert_eq!(hasher.hash("👍"), 128077 % 10000); // Thumbs up
        assert_eq!(hasher.hash("👋"), 128075 % 10000); // Waving hand
        assert_eq!(hasher.hash("🎉"), 127881 % 10000); // Party popper
        assert_eq!(hasher.hash("❤"), 10084 % 10000); // Red heart
        assert_eq!(hasher.hash("😊"), 128522 % 10000); // Unicode value for '😊' (smiling face) is 128522
        assert_eq!(hasher.hash("🚀"), 128640 % 10000); // Unicode value for '🚀' (rocket) is 128640
    }

    #[test]
    fn test_hash_polyu32_multiple_characters() {
        let config = default_config();
        let hasher = PassNameHasher::new(config);
        assert_eq!(hasher.hash("abc"), (97 + 31 * 98 + 31 * 31 * 99) % 10000);
        assert_eq!(
            hasher.hash("hello"),
            (104 + 31 * 101 + 31 * 31 * 108 + 31 * 31 * 31 * 108 + 31 * 31 * 31 * 31 * 111) % 10000
        );
    }

    #[test]
    fn test_hash_polyu32_unicode_multiple_characters() {
        let config = default_config();
        let hasher = PassNameHasher::new(config);
        assert_eq!(hasher.hash("אא"), (31 * 1488 + 1488) % 10000); // Testing two Hebrew letters
                                                                   // Testing a mix of characters
        assert_eq!(hasher.hash("אב"), (31 * 1489 + 1488) % 10000); // 'א' (Aleph) and 'ב' (Bet)

        // Testing with Unicode characters from different languages
        assert_eq!(hasher.hash("你好"), (20320 + 22909 * 31) % 10000);
        // '你' (nǐ) and '好' (hǎo)
    }

    #[test]
    fn test_hash_polyu32_long_string() {
        let config = default_config();
        let hasher = PassNameHasher::new(config);
        let long_string = "This is a longer string to test the polyu32 hash function. 😀";
        let expected_hash: u32 = long_string
            .chars()
            .fold((0u32, 1u32), |(hash, power), ch| {
                let new_hash = hash.wrapping_add(power.wrapping_mul(ch as u32));
                let new_power = power.wrapping_mul(31);
                (new_hash, new_power)
            })
            .0
            % 10000; // Get the final hash value

        assert_eq!(hasher.hash(long_string), expected_hash);
    }

    #[test]
    fn test_hash_polyu32_hash_range() {
        let config = default_config();
        let hasher = PassNameHasher::new(config);
        for i in 0..1000 {
            let input = "test".repeat(i);
            let hash_value = hasher.hash(&input);
            assert!(
                hash_value < 10000,
                "Hash value {} is out of allowed range for iteration {}, input '{}'",
                hash_value,
                i,
                input
            );
        }
    }

    #[test]
    fn test_hash_config_format_fields() {
        let formatted = test_hash_config().format_fields();
        assert_eq!(formatted, "12345.100.31.42.7");
    }

    #[test]
    fn test_hash_config_to_bytes() {
        let bytes = test_hash_config().to_bytes();

        // Check the start marker
        assert_eq!(&bytes[0..4], &PassNameHasherConfig::START_MARKER);
        // Check the end marker
        assert_eq!(&bytes[24..28], &PassNameHasherConfig::END_MARKER);
        // Check the encoded values
        assert_eq!(&bytes[4..8], &12345u32.to_le_bytes());
        assert_eq!(&bytes[8..12], &100u32.to_le_bytes());
        assert_eq!(&bytes[12..16], &31u32.to_le_bytes());
        assert_eq!(&bytes[16..20], &42u32.to_le_bytes());
        assert_eq!(&bytes[20..24], &7u32.to_le_bytes());
    }

    #[test]
    fn test_hash_config_from_bytes() {
        let config = test_hash_config();
        let bytes = config.to_bytes();
        let decoded_config = PassNameHasherConfig::from_bytes(&bytes);

        assert_eq!(decoded_config.init_hash, config.init_hash);
        assert_eq!(decoded_config.space_size, config.space_size);
        assert_eq!(decoded_config.prime, config.prime);
        assert_eq!(decoded_config.arg1, config.arg1);
        assert_eq!(decoded_config.arg2, config.arg2);
    }

    #[test]
    #[should_panic(expected = "Invalid byte data format: missing hasher config markers")]
    fn test_hash_config_from_bytes_invalid_marker() {
        let invalid_bytes = [0u8; PassNameHasherConfig::SIZE];
        PassNameHasherConfig::from_bytes(&invalid_bytes);
    }
}
