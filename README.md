# BIP85 Password Generator

Generate a password of a given length and index based on a BIP32 Root Key and the proposal to modify BIP85 found at [BIP85 Proposal](https://github.com/scgbckbone/bips/blob/passwords/bip-0085.mediawiki#PWD). This has been implemented by Coldcard, as detailed in their [documentation](https://coldcard.com/docs/bip85-passwords). Other BIP85 implementations are available at [Ian Coleman's BIP39](https://iancoleman.io/bip39/).

The derivation path is `m/83696968'/707764'/{pwd_len}'/{index}'`. The process involves Base64 encoding all 64 bytes of entropy, removing any spaces or new lines inserted by the Base64 encoding process, and slicing the Base64 result string from index 0 to `pwd_len`. This slice is the password. As `pwd_len` is limited to 86, passwords will not contain padding.

## Build

[Install Rust and Cargo](https://doc.rust-lang.org/cargo/getting-started/installation.html)

`$ curl https://sh.rustup.rs -sSf | sh`

`$ cargo build --release`

`$ cargo test build`

## Installation

`$ cargo install --path .`

cause the error[E0277]: the trait bound `&Stdin: std::io::Read` is not satisfied

`$ sudo cp ./target/release/bip85_password /usr/local/bin/passmgr`

## Usage

Supply a BIP32 Root Key or BIP-39 mnemonic phrase:

`$ bip85_password`

`Enter a BIP32 Root Key (xprv...) or English 24-word BIP39 mnemonic:`

`Enter passphrase for the BIP39 mnemonic [Empty]:`

`⧫Name of a password:`

### Options:
* `-n`, `--name <String>` Name of the password. Case insensitive (e.g., servicename@username#no).
* `-i`, `--index <u32>` Index for BIP85 (default 0).
* `-l`, `--length <u32>` Password length (default 21, must be between 0 and 86).
* `-v`, `--verbose` Enable verbose mode. Displays the password index corresponding to the given name. [default: false].
* `-c`, `--no-clipboard` Prevent copying the password to the clipboard; display it instead. [default: false].
* `-e`, `--encrypt` Encrypt the mnemonic into the database instead of decrypting it. [default: false].
* `-f`, `--file <String>` Path to the password-protected mnemonic database. Used for decrypting the mnemonic database by default. Provide an empty argument to omit the default decryption behavior. [default: ~/mnemonic.db].

### Example:
`$ bip85_password -i 0 -l 15 -c`

`xprv9s21ZrQH143K2LBWUUQRFXhucrQqBpKdRRxNVq2zBqsx8HVqFk2uYo8kmbaLLHRdqtQpUm98uKfu3vca1LqdGhUtyoFnCNkfmXRyPXLjbKb`

Generates password: `BHlnNOQWhouDSGN`

Based on [Coldcard Documentation](https://coldcard.com/docs/bip85-passwords), [BIP85 Proposal](https://github.com/scgbckbone/bips/blob/passwords/bip-0085.mediawiki#PWD), and [BIP85 Tests](https://github.com/scgbckbone/btc-hd-wallet/blob/master/tests/test_bip85.py).

## Tests

### Run Unit Tests

`$ cargo test --workspace`

### BIP32 Root Key:
`xprv9s21ZrQH143K2LBWUUQRFXhucrQqBpKdRRxNVq2zBqsx8HVqFk2uYo8kmbaLLHRdqtQpUm98uKfu3vca1LqdGhUtyoFnCNkfmXRyPXLjbKb` (from [BIP85 Proposal](https://github.com/scgbckbone/bips/blob/passwords/bip-0085.mediawiki#PWD))

- Password length 20, index 0: `RrH7uVI0XlpddCbiuYV+`
- Password length 21, index 0: `dKLoepugzdVJvdL56ogNV`
- Password length 24, index 0: `vtV6sdNQTKpuefUMOHOKwUp1`
- Password length 32, index 1234: `mBhJgXCJd6IpdOu1cc/D1wU+5sxj/1tK`
- Password length 64, index 1234: `HBqosVLBhKneX8ZCZgLdvmA8biOdUV2S/AteE5Rs8sMT0pfG3aItk/IrHGEpY9um`
- Password length 86, index 1234: `7n3VQ63qjgY6OJBQxqWYToNRfzzN5J8DwN1D8JqlZfnsF+1LdPXG3gkOXighX4iKyKip8nRIhVVVObh/G41F7g`

### BIP-39 Mnemonic:
The mnemonic from [Coldcard Documentation](https://coldcard.com/docs/bip85-passwords).

`wife shiver author away frog air rough vanish fantasy frozen noodle athlete pioneer citizen symptom firm much faith extend rare axis garment kiwi clarify`

The Coldcard Wallet only generates passwords with length 21. The mnemonic is equivalent to the BIP32 Root Key above.

- Password length 21, index 0: `m/83696968'/707764'/21'/0'`
  Generated password: `BSdrypS+J4Wr1q8DWjbFE`

- Password length 21, index 8:
  Generated password: `U4RD0R0A0RjpHOFtwnv9k`

## Notes
- The password length should be between 20 and 86 as per the specification, but the minimum length is not enforced in the implementation.
- The index must be less than 2,147,483,648 (2^31).
- Ensure that you have the necessary dependencies installed to run the password generator.

## License
This project is licensed under the MIT License. See the [LICENSE](LICENSE.md) file for details.

## Acknowledgments
- Thanks to the Coldcard team for their implementation of BIP85 passwords.
- Special thanks to Ian Coleman for his contributions to BIP39 and related tools.

For more information, please refer to the documentation and resources linked above.
